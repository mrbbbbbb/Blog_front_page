# 博客前端

#### 介绍
个人博客的前端页面，后续会进行页面的调整，目前美化仅做了一部分。

#### 使用说明

  仅限于自己学习，不得商用等

#### 使用到的技术以及插件
 - html javascript css(不用说)
 - jquery
 - sermantic UI
 - editormd
 - qrcode  二维码生成
 - tocbot  目录生成
 - typo.css  文章排版
 - prism   代码块
 - waypoints
  
#### 参与贡献

1. mrbbbbbb

#### 部分展视

![index页面](https://images.gitee.com/uploads/images/2020/0605/185818_edbb9634_7487570.png "屏幕截图.png")

![博客评论](https://images.gitee.com/uploads/images/2020/0605/190151_d7e64bfd_7487570.png "屏幕截图.png")

![集成markdown](https://images.gitee.com/uploads/images/2020/0605/192117_6de90f31_7487570.png "屏幕截图.png")
